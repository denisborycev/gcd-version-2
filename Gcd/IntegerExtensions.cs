﻿using System;
using System.Diagnostics;
using System.Threading;

namespace Gcd
{
    public static class IntegerExtensions
    {
        public static int GcdE(int a, int b)
        {
            if (b == 0)
            {
                return a;
            }
            else
            {
                return GcdE(b, a % b);
            }
        }

        public static int GcdS(int a, int b)
        {
            if (a == 0)
            {
                return b;
            }

            if (b == 0)
            {
                return a;
            }

            if (a == b)
            {
                return a;
            }

            if (a == 1 || b == 1)
            {
                return 1;
            }

            if ((a & 1) == 0)
            {
                return ((b & 1) == 0) ? GcdS(a >> 1, b >> 1) << 1 : GcdS(a >> 1, b);
            }
            else
            {
                return ((b & 1) == 0) ? GcdS(a, b >> 1) : GcdS(b, a > b ? a - b : b - a);
            }
        }

        public static int GetGcdByEuclidean(int a, int b)
        {
            if (a == 0 && b == 0)
            {
                throw new ArgumentException("All numbers cannot be 0 at the same time.");
            }

            if (a == int.MinValue || b == int.MinValue)
            {
                throw new ArgumentOutOfRangeException($"Number cannot be {int.MinValue}.");
            }

            a = Math.Abs(a);
            b = Math.Abs(b);
            return GcdE(a, b);
        }

        public static int GetGcdByEuclidean(int a, int b, int c)
        {
            if (a == 0 && b == 0 && c == 0)
            {
                throw new ArgumentException("All numbers cannot be 0 at the same time.");
            }

            if (a == int.MinValue || b == int.MinValue || c == int.MinValue)
            {
                throw new ArgumentOutOfRangeException($"Number cannot be {int.MinValue}.");
            }

            a = Math.Abs(a);
            b = Math.Abs(b);
            c = Math.Abs(c);
            return GcdE(GcdE(a, b), c);
        }

        public static int GetGcdByEuclidean(int a, int b, params int[] other)
        {
            if (a == 0 && b == 0)
            {
                throw new ArgumentException("All numbers cannot be 0 at the same time.");
            }

            if (a == int.MinValue || b == int.MinValue)
            {
                throw new ArgumentOutOfRangeException($"Number cannot be {int.MinValue}.");
            }

            a = Math.Abs(a);
            b = Math.Abs(b);
            a = GcdE(a, b);
            for (int i = 0; i < other.Length; i++)
            {
                int c = Math.Abs(other[i]);
                a = GcdE(a, c);
            }

            return a;
        }

        public static int GetGcdByStein(int a, int b)
        {
            if (a == int.MinValue || b == int.MinValue)
            {
                throw new ArgumentOutOfRangeException($"Number cannot be {int.MinValue}.");
            }

            if (a == 0 && b == 0)
            {
                throw new ArgumentException("All numbers cannot be 0 at the same time.");
            }

            a = Math.Abs(a);
            b = Math.Abs(b);
            return GcdS(a, b);
        }

        public static int GetGcdByStein(int a, int b, int c)
        {
            if (a == 0 && b == 0 && c == 0)
            {
                throw new ArgumentException("All numbers cannot be 0 at the same time.");
            }

            if (a == int.MinValue || b == int.MinValue || c == int.MinValue)
            {
                throw new ArgumentOutOfRangeException($"Number cannot be {int.MinValue}.");
            }

            a = Math.Abs(a);
            b = Math.Abs(b);
            c = Math.Abs(c);
            return GcdS(GcdS(a, b), c);
        }

        public static int GetGcdByStein(int a, int b, params int[] other)
        {
            bool flag = true;
            for (int i = 0; i < other.Length; i++)
            {
                if (other[i] != 0)
                {
                    flag = false;
                    break;
                }
            }

            if (a == 0 && b == 0 && flag)
            {
                throw new ArgumentException("All numbers cannot be 0 at the same time.");
            }

            if (a == int.MinValue || b == int.MinValue)
            {
                throw new ArgumentOutOfRangeException($"Number cannot be {int.MinValue}.");
            }

            a = Math.Abs(a);
            b = Math.Abs(b);
            a = GcdS(a, b);
            for (int i = 0; i < other.Length; i++)
            {
                a = GcdS(a, other[i]);
            }

            return a;
        }

        public static int GetGcdByEuclidean(out long elapsedTicks, int a, int b)
        {
            if (a == 0 && b == 0)
            {
                throw new ArgumentException("All numbers cannot be 0 at the same time.");
            }

            if (a == int.MinValue || b == int.MinValue)
            {
                throw new ArgumentOutOfRangeException($"Number cannot be {int.MinValue}.");
            }

            Stopwatch sw = new Stopwatch();
            sw.Start();
            a = Math.Abs(a);
            b = Math.Abs(b);
            int s = GetGcdByEuclidean(a, b);
            sw.Stop();
            elapsedTicks = sw.ElapsedTicks;
            return s;
        }

        public static int GetGcdByEuclidean(out long elapsedTicks, int a, int b, int c)
        {
            if (a == 0 && b == 0 && c == 0)
            {
                throw new ArgumentException("All numbers cannot be 0 at the same time.");
            }

            if (a == int.MinValue || b == int.MinValue)
            {
                throw new ArgumentOutOfRangeException($"Number cannot be {int.MinValue}.");
            }

            a = Math.Abs(a);
            b = Math.Abs(b);
            Stopwatch sw = new Stopwatch();
            sw.Start();
            int s = GetGcdByEuclidean(GetGcdByEuclidean(a, b), c);
            sw.Stop();
            elapsedTicks = sw.ElapsedTicks;
            return s;
        }

        public static int GetGcdByEuclidean(out long elapsedTicks, int a, int b, params int[] other)
        {
            if (a == 0 && b == 0)
            {
                throw new ArgumentException("All numbers cannot be 0 at the same time.");
            }

            if (a == int.MinValue || b == int.MinValue)
            {
                throw new ArgumentOutOfRangeException($"Number cannot be {int.MinValue}.");
            }

            a = Math.Abs(a);
            b = Math.Abs(b);
            Stopwatch sw = new Stopwatch();
            sw.Start();
            int s = GetGcdByEuclidean(a, b, other);
            sw.Stop();
            elapsedTicks = sw.ElapsedTicks;
            return s;
        }

        public static int GetGcdByStein(out long elapsedTicks, int a, int b)
        {
            if (a == 0 && b == 0)
            {
                throw new ArgumentException("All numbers cannot be 0 at the same time.");
            }

            if (a == int.MinValue || b == int.MinValue)
            {
                throw new ArgumentOutOfRangeException($"Number cannot be {int.MinValue}.");
            }

            a = Math.Abs(a);
            b = Math.Abs(b);
            Stopwatch sw = new Stopwatch();
            sw.Start();
            int s = GetGcdByStein(a, b);
            sw.Stop();
            elapsedTicks = sw.ElapsedTicks;
            return s;
        }

        public static int GetGcdByStein(out long elapsedTicks, int a, int b, int c)
        {
            if (a == 0 && b == 0 && c == 0)
            {
                throw new ArgumentException("All numbers cannot be 0 at the same time.");
            }

            if (a == int.MinValue || b == int.MinValue)
            {
                throw new ArgumentOutOfRangeException($"Number cannot be {int.MinValue}.");
            }

            a = Math.Abs(a);
            b = Math.Abs(b);
            Stopwatch sw = new Stopwatch();
            sw.Start();
            int s = GetGcdByStein(GetGcdByStein(a, b), c);
            sw.Stop();
            elapsedTicks = sw.ElapsedTicks;
            return s;
        }

        public static int GetGcdByStein(out long elapsedTicks, int a, int b, params int[] other)
        {
            if (a == 0 && b == 0)
            {
                throw new ArgumentException("All numbers cannot be 0 at the same time.");
            }

            if (a == int.MinValue || b == int.MinValue)
            {
                throw new ArgumentOutOfRangeException($"Number cannot be {int.MinValue}.");
            }

            a = Math.Abs(a);
            b = Math.Abs(b);
            Stopwatch sw = new Stopwatch();
            sw.Start();
            int s = GetGcdByStein(a, b, other);
            sw.Stop();
            elapsedTicks = sw.ElapsedTicks;
            return s;
        }
    }
}
